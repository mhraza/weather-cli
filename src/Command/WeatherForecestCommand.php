<?php

namespace App\Command;

use App\Services\WeatherForecast\WeatherForecastService;
use App\Services\WeatherForecast\WeatherForecastServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'WeatherForecestCommand',
    description: 'Add a short description for your command',
)]
class WeatherForecestCommand extends Command
{
    protected static $defaultName = 'app:weather-cli';


    public function __construct(
        private WeatherForecastServiceInterface $weatherForecastService,
        string $name = null
    )
    {
        parent::__construct($name);
    }


    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('cityName', InputArgument::REQUIRED, 'Enter city name')
            ->addOption(
                'cityNameOption',
                null,
                InputOption::VALUE_NONE,
                'Enter the city name for which you want to see forecest'
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io       = new SymfonyStyle($input, $output);
        $cityName = $input->getArgument('cityName');

        if ($cityName) {
            $io->note(sprintf('You passed city name: %s', $cityName));
        }

        $forecast = $this->weatherForecastService->getNextThreeDaysForecastByCity($cityName);

        foreach ($forecast as $dayForecast) {

            $output->writeln(sprintf('Date: %s', $dayForecast[WeatherForecastService::FORECAST_DATE]));
            $output->writeln(sprintf('Temperature: %s', $dayForecast[WeatherForecastService::FORECAST_AVERAGE_TEMPERATURE]));
            $output->writeln(sprintf('Condition: %s', $dayForecast[WeatherForecastService::FORECAST_CONDITION]));
            $output->writeln('--------------------------------------');
        }

        $io->success('Please review the result !');

        return Command::SUCCESS;
    }
}
