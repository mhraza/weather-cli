<?php


namespace App\Services\WeatherForecast;


interface WeatherForecastServiceInterface
{
    /**
     * @param string $cityName
     *
     * @return array
     */
    public function getNextThreeDaysForecastByCity(string $cityName): array;

}