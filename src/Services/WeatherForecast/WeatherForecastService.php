<?php


namespace App\Services\WeatherForecast;


use App\Services\WeatherApiClient\WeatherApiClientServiceInterface;

class WeatherForecastService implements WeatherForecastServiceInterface
{
    const WEATHER_FORECAST_URL         = '/v1/forecast.json';
    const API_KEY                      = 'key';
    const CITY_NAME                    = 'q';
    const FORECAST                     = 'forecast';
    const FORECAST_DAY                 = 'forecastday';
    const FORECAST_DATE                = 'date';
    const FORECAST_DAY_OBJECT          = 'day';
    const FORECAST_AVERAGE_TEMPERATURE = 'avgtemp_c';
    const FORECAST_CONDITION           = 'condition';
    const FORECAST_CONDITION_TEXT      = 'text';
    const PARAMS_DAYS_TEXT             = 'days';
    const MAX_DAYS                     = 3;


    public function __construct(
        private WeatherApiClientServiceInterface $weatherApiClientService
    )
    {
    }


    /**
     * @param string $cityName
     *
     * @return array
     */
    public function getNextThreeDaysForecastByCity(string $cityName): array
    {
        //This key should be somewhere in the config, it should not be here
        $params = [
            self::API_KEY          => '74bb18bd1d9a4293a5194538212705',
            self::CITY_NAME        => $cityName,
            self::PARAMS_DAYS_TEXT => self::MAX_DAYS
        ];

        $result = $this->weatherApiClientService->get(self::WEATHER_FORECAST_URL, $params);

        return $this->extractRequiredForecastData($result);
    }


    /**
     * @param array $forecastData
     *
     * @return array
     */
    private function extractRequiredForecastData(array $forecastData): array
    {
        if (!isset($forecastData[self::FORECAST][self::FORECAST_DAY])) {

            return [];

        }

        $result = [];

        foreach ($forecastData[self::FORECAST][self::FORECAST_DAY] as $dayForecast) {

            $data                                     = [];
            $data[self::FORECAST_DATE]                = $dayForecast[self::FORECAST_DATE] ?? '';
            $data[self::FORECAST_AVERAGE_TEMPERATURE] =
                $dayForecast[self::FORECAST_DAY_OBJECT][self::FORECAST_AVERAGE_TEMPERATURE] ?? '';
            $data[self::FORECAST_CONDITION]           =
                $dayForecast[self::FORECAST_DAY_OBJECT][self::FORECAST_CONDITION][self::FORECAST_CONDITION_TEXT] ?? '';

            $result[] = $data;

        }

        return $result;
    }
}