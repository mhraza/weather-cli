# KW-commerence
Weather Cli

## Approach

#### I can do it with docker but no time available :)
#### I have tried to focus on maintainable, extendable and simpler implementation instead of highly coupled code.
#### I have used symfony 5.3 console application.


## 1 Installation

#### git clone https://naeem_nasir_123@bitbucket.org/naeem_nasir_123/weather-cli.git

## 2 Requirements

#### 2.1. PHP 8 is needed to run this project
#### 2.2. Please add all the PHP extension that symfony 5.3 needed.

## 3. Install composer packages:

$ composer install

## 4 Run the following console command:

$ php bin/console app:weather-cli Lahore

## 5 Run unittests:

$ ./vendor/bin/phpunit tests
